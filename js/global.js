//SOUNDS
var audio = [],
	audioPiece = [];

//TIMEOUTS
var timeout = [],
	launch = [],
	sprites = [],
	typingText = [],
	globalName;

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum),
			preLoadImage = $("#pre-load .pre-load-image");
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			preLoadImage.css("width", loadPercentage + "%");
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

var soundToZero = function(audiofile, vol)
{
	if(vol >= 0)
	{
		audiofile.volume = vol/10;
		vol --;
		timeout[0] = setTimeout(function(){
			soundToZero(audiofile, vol);
		}, 1000);
	}
}	

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
}

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var setMenuStuff = function()
{
	var regBox = $(".reg-box"),
		error = $(".error"),
		enterButton = $(".enter-button"),
		password = $(".password"),
		name = $(".name"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button");
		
	name.val("");
	password.val("");
	
	var sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");

	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
}

launch["frame-000"] = function(){
		theFrame = $("#frame-000"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		menuRoll = $(prefix + ".menu-roll"),
		menuRollFinal = $(prefix + ".menu-roll-final"),
		chestOpen = $(prefix + ".chest-open"),
		chestClosed = $(prefix + ".chest-closed"),
		button = $(prefix + ".button");
	
	
	
	sprites[0] = new Motio(menuRoll[0], {
		fps: 4,
		frames: 10
	});
	
	sprites[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
		
	menuRollFinal.fadeOut(0);
	bg2.fadeOut(0);
	chestOpen.fadeOut(0);
	chestClosed.fadeOut(0);
	
	audio[0] = new Audio("audio/main-theme.mp3");
	audio[1] = new Audio("audio/chest-close.mp3");
	
	var buttonListener = function(){
		audio[0].play();
		button.fadeOut(1000);
	
		//chestCl.fadeOut(0);
		timeout[0] = setTimeout(function(){
			sprites[0].play();
		}, 2500);
		timeout[1] = setTimeout(function(){
			menuRoll.fadeOut(0);
			menuRollFinal.fadeIn(0);
		}, 4200);
		timeout[2] = setTimeout(function(){
			menuRollFinal.fadeOut(1000);
		}, 5000);
		timeout[2] = setTimeout(function(){
			chestOpen.fadeIn(1000);
		}, 7000);
		timeout[3] = setTimeout(function(){
			chestOpen.addClass("chest-open-big");
			bg1.fadeOut(1000);
			bg2.fadeIn(1000);
		}, 8000);
		timeout[4] = setTimeout(function(){
		}, 10000);
		timeout[5] = setTimeout(function(){
			chestOpen.fadeOut(0);
			chestClosed.fadeIn(0);
			audio[1].play();
		}, 13000);
		timeout[6] = setTimeout(function(){
			chestClosed.addClass("chest-closed-small");
		}, 16000);
		timeout[7] = setTimeout(function(){
			fadeNavsInAuto();
			soundToZero(audio[0], 3);
		}, 22000);
	};
	
	button.off("click", buttonListener);
	button.on("click", buttonListener);
	
	startButtonListener = buttonListener;
}

launch["frame-001"] = function(){
		theFrame = $("#frame-001"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		doorOpen = $(prefix + ".door-open"), 
		doorClosed = $(prefix + ".door-closed"),
		boy = $(prefix + ".boy"),
		lockH = $(prefix + ".lock-highlighted"), 
		methodLabel = $(prefix + ".method-label"),
		methodLabelH = $(prefix + ".method-label-highlighted"),
		bagsAndBoxes = $(prefix + ".bags-and-boxes"),
		bagsAndBoxesH = $(prefix + ".bags-and-boxes-highlighted"),
		boySprite = new Motio(boy[0], {
			fps: 5,
			frames: 2
		});
	
	lockH.fadeOut(0);
	methodLabelH.fadeOut(0);
	bagsAndBoxesH.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s2-1.mp3");
	audio[1] = new Audio("audio/s3-1.mp3");
		
	doorOpen.fadeOut(0);
	
	var doneSecond = 0;	
	audio[1].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime);
		if(currTime === 1 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			lockH.fadeIn(500);
			timeout[1] = setTimeout(function(){
				lockH.fadeOut(0);
			}, 2000);
		}
		else if(currTime === 4 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bagsAndBoxesH.fadeIn(500);
			boy.css("opacity", "0.2");
			timeout[2] = setTimeout(function(){
				bagsAndBoxesH.fadeOut(0);
				boy.css("opacity", "");
				methodLabelH.fadeIn(500);
			}, 2000);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();
	});
	
	audio[1].addEventListener("ended", function(){
		boySprite.pause();
		boySprite.to(0, true);
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 2000);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-002"] = function(){
		theFrame = $("#frame-002"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		doorOpen = $(prefix + ".door-open"), 
		doorClosed = $(prefix + ".door-closed"),
		boy = $(prefix + ".boy"),
		methodLabel = $(prefix + ".method-label"),
		bagsAndBoxes = $(prefix + ".bags-and-boxes"),
		whiteCloud = $(prefix + ".white-cloud"), 
		apples = $(prefix + ".apples"),
		coins = $(prefix + ".coins"),
		candies = $(prefix + ".candies"),
		appleMolecule = $(prefix + ".apple-molecule");
	
	audio[0] = new Audio("audio/s3-2.mp3"),
	
	boySprite = new Motio(boy[0], {
		fps: 5,
		frames: 2
	});
	
	appleMoleculeSprite = new Motio(appleMolecule[0], {
		fps: 1,
		frames: 5
	});
	
	appleMoleculeSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	whiteCloud.fadeOut(0);
	coins.fadeOut(0);
	candies.fadeOut(0);
	apples.fadeOut(0);
	appleMolecule.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime !== doneSecond)
		{
			coins.fadeOut(500);
			candies.fadeOut(500);
			apples.animate({
				"background-size": "1500%"}, 4000, function(){
					appleMolecule.fadeIn(1000);
					appleMoleculeSprite.play();
				});
		}		
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		timeout[4] = setTimeout(function(){
			apples.fadeOut(500);
			whiteCloud.fadeOut(500);
			appleMolecule.fadeOut(500);
			boySprite.to(0, true);
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(2);
		}, 3000);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		
		timeout[1] = setTimeout(function(){
			whiteCloud.fadeIn(0);
			apples.fadeIn(500);
		}, 2000);
		timeout[2] = setTimeout(function(){
			candies.fadeIn(500);
		}, 3000);
		timeout[3] = setTimeout(function(){
			coins.fadeIn(500);
		}, 4000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-003"] = function(){
		theFrame = $("#frame-003"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		woodLabel = $(prefix + ".wood-label"),
		woodExit = $(prefix + ".wood-exit"),
		doorOpen = $(prefix + ".door-open"), 
		doorClosed = $(prefix + ".door-closed"),
		boy = $(prefix + ".boy"),
		methodLabel = $(prefix + ".method-label"),
		methodLabelH = $(prefix + ".method-label-highlighted"),
		scales = $(prefix + ".scales"),
		scalesHighlighted = $(prefix + ".scales-highlighted"),
		bag1 = $(prefix + ".bag-1"),
		bag1Highlighted = $(prefix + ".bag-1-highlighted"),
		bag2 = $(prefix + ".bag-2"),
		bag2Highlighted = $(prefix + ".bag-2-highlighted"),
		key = $(prefix + ".key"),
		answerBags = $(prefix + ".answer-bag");
	
	audio[0] = new Audio("audio/s3-3.mp3");
	audio[1] = new Audio("audio/scale-sound.mp3");
	audio[2] = new Audio("audio/door-open.mp3");
	audio[3] = new Audio("audio/footsteps.mp3");
	
	/*PRELOADING SOME IMAGES*/
	var img1 = new Image();
	img1.src = "pics/bag-2-highlighted.png";
	
	var img2 = new Image();
	img2.src = "pics/answer-bag-1.png";
	
	var img3 = new Image();
	img3.src = "pics/answer-bag-2.png";
	
	var img4 = new Image();
	img4.src = "pics/answer-bag-3.png";
	
	var boySprite = new Motio(boy[0], {
		fps: 5,
		frames: 2
	}),
	scalesSprite = new Motio(scales[0], {
		fps: 4,
		frames: 5
	});
	
	woodExitSprite = new Motio(woodExit[0], {
		fps: 4, 
		frames: 8
	});
	
	scalesSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	woodExitSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(3);
		}
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	doorOpen.fadeOut(0);
	answerBags.fadeOut(0);
	key.fadeOut(0);
	woodExit.fadeOut(0);
	bag1Highlighted.fadeOut(0);
	bag2Highlighted.fadeOut(0);
	scalesHighlighted.fadeOut(0);
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 10 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bag2.fadeOut(0);
			bag2Highlighted.fadeIn(0);
		}
		else if(currTime === 18 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bag2.fadeIn(0);
			bag2Highlighted.fadeOut(0);
			scales.fadeOut(0);
			scalesHighlighted.fadeIn(0);
		}
		else if(currTime === 21 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			scales.fadeIn(0);
			scalesHighlighted.fadeOut(0);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		timeout[0] = setTimeout(function(){
			bag1.fadeOut(0);
			bag1Highlighted.fadeIn(0);
			timeout[1] = setTimeout(function(){
				bag1.fadeIn(0);
				bag1Highlighted.fadeOut(0);
			}, 5000);
		}, 2000);
	}
	
	var answerBagListener = function(){
		if($(this).attr("data-correct"))
		{
			key.fadeIn(0);
			scales.fadeOut(0);
			key.addClass("transition-2s");
			key.css("top", "45%");
			key.css("left", "20%");
			timeout[4] = setTimeout(function(){
				doorClosed.fadeOut(0);
				doorOpen.fadeIn(0);
				key.fadeOut(500);
				audio[2].play();
				bag1.fadeOut(0);
				bag2.fadeOut(0);
				methodLabel.fadeOut(0);
				methodLabelH.fadeOut(0);
				woodLabel.fadeOut(0);
				answerBags.fadeOut(0);
				timeout[5] = setTimeout(function(){
					doorOpen.fadeOut(0);
					woodExit.fadeIn(0);
					woodExitSprite.play();
					audio[3].play();
				}, 2000);
			}, 2000);
		}
	}
	
	answerBags.off("click", answerBagListener);
	answerBags.on("click", answerBagListener);
	
	var draggableBag = $("#frame-003 .draggable-ball");
	draggabillyBag = new Draggabilly(draggableBag[0]);
	var vegetable, basket;
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
		vegetable.css("background-image", "url('pics/bag-2-highlighted.png')");
	};
	var onEnd = function(instance, event, pointer){
		vegetable.css("background-image", "");
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(basket.hasClass("scales-basket"))
		{
			scalesSprite.play();
			audio[1].play();
			timeout[0] = setTimeout(function(){
				answerBags.fadeIn(0);
				boy.fadeOut(0);
			}, 2000);
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.fadeIn(0);
		}
	};
	
	draggabillyBag.on("dragStart", onStart);
	draggabillyBag.on("dragEnd", onEnd);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-004"] = function(){
		theFrame = $("#frame-004"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open"),
		sandScheme = $(prefix + ".sand-scheme"),
		sioAnimated = $(prefix + ".sio-animated");
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	sioAnimatedSprite = new Motio(sioAnimated[0], {
		fps: 1,
		frames: 5
	});
	
	sioAnimatedSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	sandScheme.fadeOut(0);	
	doorOpen.fadeOut(0);
	sioAnimated.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s4-1.mp3");
	audio[1] = new Audio("audio/s4-2.mp3");
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();
		
		timeout[1] = setTimeout(function(){
			sandScheme.fadeOut(0);
		}, 18000);
		timeout[2] = setTimeout(function(){
			sioAnimated.fadeIn(500);
			sioAnimatedSprite.play();
		}, 20000);
	});
	
	audio[1].addEventListener("ended", function(){
		boySprite.pause();
		theFrame.attr("data-done", "true"); 
		fadeNavsInAuto();
		sendCompletedStatement(4);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play(0);
		timeout[0] = setTimeout(function(){
			sandScheme.fadeIn(500);
		}, 1000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-005"] = function(){
		theFrame = $("#frame-005"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open"),
		taskLabel = $(prefix + ".task-label"),
		correct = $(prefix + ".correct"),
		keepThinking = $(prefix + ".keep-thinking"),
		stones = $(prefix + ".stone"),
		equalSign = $(prefix + ".equal-sign"),
		bucketC = $(prefix + ".bucket-c"),
		bucketO2 = $(prefix + ".bucket-o2"),
		bucketCO = $(prefix + ".bucket-co"),
		buckets = $(prefix + ".bucket");
		
		num = [];
		num["c"] = 1, 
		num["o2"] = 1, 
		num["co"] = 1;
		
		label = [];
		label["c"] = "C",
		label["o2"] = "O<sub>2</sub>",
		label["co"] = "CO",
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	correct.fadeOut(0);
	keepThinking.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	sprite = [];
	
	sprite["c"] = new Motio(bucketC[0], {
		"frames": "4",
		"fps": "1"
	});
	
	sprite["o2"] = new Motio(bucketO2[0], {
		"frames": "4",
		"fps": "1"
	});
	
	sprite["co"] = new Motio(bucketCO[0], {
		"frames": "4",
		"fps": "1"
	});
		
	audioPiece[0] = new AudioPiece("s4-3", 0, 24);
	audioPiece[1] = new AudioPiece("s4-3", 27, 33);
	audio[0] = new Audio("audio/stone-sound.mp3");
	
	var bucketsListener = function(){
		var currElem = $(this),
			key = currElem.attr("data-key");
		if(num[key] > 1)
		{
			audio[0].play();
			num[key] --;
		}
		
		var currLabel = currElem.children().filter("label");
		
		if(num[key] === 1)	
			currLabel.html(label[key]);
		else
			currLabel.html(num[key] + label[key]);
	};
	buckets.off("click", bucketsListener);
	buckets.on("click", bucketsListener);
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	}
	var successFunction = function(vegetable, basket){
		audio[0].play();
		var key = basket.attr("data-key"),
			frame = sprite[key].frame;
			
		if(frame <= 4)
		{
			frame++;
			num[key] ++;
			var elem = basket.children().filter("label");
			elem.html(num[key] + label[key]);
		}
		
		sprite[key].to(frame, true);
		vegetable.css("left", "");
		vegetable.css("top", "");
	}
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
	}
	var finishCondition = function(){
		
	}
	var finishFunction = function(){
		
	}
	
	var dragTask = new DragTask(stones, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	var equalSignListener = function(){
		if(num["c"] === 2 && num["co"] == 2 && num["o2"] === 1)
		{
			correct.fadeIn(500);
			audioPiece[1].play();
			boy.fadeIn(500);
			boySprite.play();
			timeout[0] = setTimeout(function(){
				boySprite.pause();
				correct.fadeOut(0);
				fadeNavsInAuto();
				sendCompletedStatement(5);
			}, 5000);
		}
		else
		{
			keepThinking.fadeIn(500);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(500);
			}, 2000);
		}
	};
	equalSign.off("click", equalSignListener);
	equalSign.on("click", equalSignListener);
	
	startButtonListener = function(){
		audioPiece[0].play();
		timeout[0] = setTimeout(function(){
			boySprite.play();
		}, 500);
		timeout[1] = setTimeout(function(){
			equalSign.css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
		}, 15000);
		timeout[2] = setTimeout(function(){
			equalSign.css("text-shadow", "");
		}, 17000);
		timeout[3] = setTimeout(function(){
			boySprite.pause();
			boy.fadeOut(500);
		}, 19000);
	}

	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-006"] = function(){
		theFrame = $("#frame-006"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		gramPerMole = $(prefix + ".gram-per-mole"),
		mole = $(prefix + ".mole"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open");
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	gramPerMole.fadeOut(0);
	mole.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s4-41.mp3");
	
	audio[0].addEventListener("ended", function(){
		gramPerMole.fadeOut(500);
		mole.fadeIn(500);
		boySprite.pause();
		timeout[0] = setTimeout(function(){
			mole.fadeOut(500);
		}, 4000);
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			hideEverythingBut($("#frame-007"));
			sendComletedStatement(6);
		}, 5000);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		gramPerMole.fadeIn(500);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-007"] = function(){
		theFrame = $("#frame-007"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open"),
		molarMassKz = $(prefix + ".molar-mass-kz"), 
		molarMassRu = $(prefix + ".molar-mass-ru"),
		molarMassEn = $(prefix + ".molar-mass-en"),
		gramPerMole = $(prefix + ".gram-per-mole");
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	gramPerMole.fadeOut(0);
	molarMassKz.fadeOut(0), 
	molarMassRu.fadeOut(0),
	molarMassEn.fadeOut(0);
	gramPerMole.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s4-42.mp3");
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		
		timeout[0] = setTimeout(function(){
			molarMassKz.fadeIn(0);
		}, 7000);
		timeout[1] = setTimeout(function(){
			molarMassRu.fadeIn(500);
		}, 8000);
		timeout[2] = setTimeout(function(){
			molarMassEn.fadeIn(1000);
		}, 9000);
		timeout[3] = setTimeout(function(){
			molarMassKz.fadeOut(0), 
			molarMassRu.fadeOut(0),
			molarMassEn.fadeOut(0);
			gramPerMole.fadeIn(2000);
		}, 12000);
		timeout[4] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendLaunchedStatement(7)
		}, 17000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-008"] = function(){
		theFrame = $("#frame-008"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		allQuestionAnswers = $(prefix + " .task, #" + theFrame.attr("id") + " .answer"), 
		task1 = $(prefix + " .task-1"),
		task2 = $(prefix + " .task-2"),
		task3 = $(prefix + " .task-3"),
		task4 = $(prefix + " .task-4"),
		task5 = $(prefix + " .task-5"),
		answers = $(prefix + " .answer"),
		result = $(prefix + " .result"),
		activeQuestion = 1,
		correct = 0;
	
	var boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	allQuestionAnswers.fadeOut(0);
	result.fadeOut(0);
	
	audio[0] = new Audio("audio/s4-5.mp3");
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		timeout[1] = setTimeout(function(){
			audio[0].pause();
			boySprite.pause();
			boy.fadeOut(1000);
			$(prefix + ".task-" + activeQuestion).fadeIn(500);
		}, 3500);
	}
	
	var answerListener = function(){
		if($(this).hasClass("true"))
		{
			correct ++;
			$(this).css("color", "green");
			$(this).css("font-weight", "bold");
		}
		else
		{
			$(this).css("color", "red");
			$(this).css("font-weight", "bold");
		}
		
		timeout[0] = setTimeout(function(){
			$(prefix + ".task-" + activeQuestion + ".true").css("color", "green");
			$(prefix + ".task-" + activeQuestion + ".true").css("font-weight", "bold");
		}, 2000);
		timeout[0] = setTimeout(function(){
			activeQuestion++;
			allQuestionAnswers.fadeOut(0);
			$(prefix + " .task-" + activeQuestion).fadeIn(0);	
		}, 4000);
		timeout[0] = setTimeout(function(){
			if(activeQuestion > 5)
			{
				$(prefix + " .task-6").html("Дұрыс жауаптар саны: " + correct);
				
				timeout[1] = setTimeout(function(){
					theFrame.attr("data-done", "true"); 
					sendCompletedStatement(8);
					fadeNavsInAuto();
				}, 6000);
			}
		}, 5000);
	};
	
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-009"] = function(){
		theFrame = $("#frame-009"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		avogadroNumber = $(prefix + " .avogadro-number"),
		avogadroKaz = $(prefix + " .avogadro-kaz"),
		avogadroRus = $(prefix + " .avogadro-rus"),
		avogadroEng = $(prefix + " .avogadro-eng"),
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	avogadroNumber.fadeOut(0);
	avogadroKaz.fadeOut(0);
	avogadroRus.fadeOut(0);
	avogadroEng.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s4-44.mp3");
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		timeout[1] = setTimeout(function(){
			avogadroNumber.fadeIn(500);
		}, 6000);
		timeout[2] = setTimeout(function(){
			avogadroNumber.fadeOut(0);
			avogadroKaz.fadeIn(500);
		}, 16000);
		timeout[3] = setTimeout(function(){
			avogadroRus.fadeIn(500);
		}, 17000);
		timeout[4] = setTimeout(function(){
			avogadroEng.fadeIn(500);
		}, 18000);
		timeout[5] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			hideEverythingBut($("#frame-010"));
			sendCompletedStatement(9);
		}, 21000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-010"] = function(){
		theFrame = $("#frame-010"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		bucket1 = $(prefix + " .bucket-1"),
		bucket2 = $(prefix + " .bucket-2"),
		bucket3 = $(prefix + " .bucket-3"),
		bucket4 = $(prefix + " .bucket-4"),
		key = $(prefix + " .key"),
		sandExit = $(prefix + " .sand-exit");
	
	var boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	var sandExitSprite = new Motio(sandExit[0], {
		fps: 3,
		frames: 7
	});
	
	sandExitSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(10);
		}
	});
	
	doorOpen.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	key.fadeOut(0);
	sandExit.fadeOut(0);
	
	audio[0] = new Audio("audio/s4-5.mp3");
	audio[1] = new Audio("audio/door-open.mp3");
	audio[2] = new Audio("audio/stone-sound.mp3");
	audio[3] = new Audio("audio/footsteps.mp3");
	audioPiece[0] = new AudioPiece("s4-5", 28, 34)
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		
		timeout[1] = setTimeout(function(){
			boySprite.pause();
		}, 18000);
		timeout[2] = setTimeout(function(){
			boySprite.play();
		}, 19000);
		timeout[3] = setTimeout(function(){
			audio[0].pause();
			boySprite.pause();
			boy.fadeOut(500);
		}, 24000);
	}
	
	var draggable = $("#frame-010 .stone"),
		draggabilly = [];
	for (var i = 0; i < draggable.length; i++)
	{
		draggabilly[i] = new Draggabilly(draggable[i]);
	}
	
	var vegetable, vegetableClone, basket,
		bucket1Num = 0, bucket2Num = 0, bucket3Num = 0, bucket4Num = 0;
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
	};
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(basket.attr("data-key") === vegetable.attr("data-key"))
		{
			switch(basket.attr("data-key"))
			{
				case "m1": bucket1Num++;
						   bucket1.css("background-image", "url(pics/bucket-label-1.png), url('pics/bucket-lvl-"+bucket1Num+".png')");
							break;
				case "m2": bucket2Num++;
							if(bucket2Num == 4) bucket2Num --;
							bucket2.css("background-image", "url(pics/bucket-label-2.png), url('pics/bucket-lvl-"+bucket2Num+".png')");
							break;
				case "m3": bucket3Num++;
							bucket3.css("background-image", "url(pics/bucket-label-3.png), url('pics/bucket-lvl-"+bucket3Num+".png')");
							break;
				case "m6": bucket4Num++;
							bucket4.css("background-image", "url(pics/bucket-label-4.png), url('pics/bucket-lvl-"+bucket4Num+".png')");
							break;
			}
			audio[2].play();
			vegetable.remove();
			if(!$(prefix + " .stone").length)
			{
				bucket1.fadeOut(0);
				bucket2.fadeOut(0);
				bucket3.fadeOut(0);
				bucket4.fadeOut(0);
				key.fadeIn(0);
				key.addClass("transition-2s");
				audioPiece[0].play();
				timeout[0] = setTimeout(function(){
					key.css("left", "10%");
					key.css("top", "45%");
					timeout[1] = setTimeout(function(){
						sandExit.fadeIn(0);
						audio[1].play();
						timeout[2] = setTimeout(function(){
							audio[3].play();
							sandExitSprite.play();
						}, 1000);
					}, 2000);
				}, 2000);
			}
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.fadeIn(0);
		}
	};
	
	for(var i = 0; i < draggabilly.length; i++)
	{
		draggabilly[i].on("dragStart", onStart);
		draggabilly[i].on("dragEnd", onEnd);
	}

	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-011"] = function(){
		theFrame = $("#frame-011"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		sugar = $(prefix + " .sugar"),
		carbonDioxide = $(prefix + " .carbon-dioxide"),
		hydrogenDioxide = $(prefix + " .hydrogen-dioxide"), 
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	sugar.fadeOut(0);
	carbonDioxide.fadeOut(0);
	hydrogenDioxide.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		timeout[0] = setTimeout(function(){
			sugar.fadeIn(500);
		}, 6000);
		timeout[1] = setTimeout(function(){
			carbonDioxide.fadeIn(500);
		}, 7000);
		timeout[2] = setTimeout(function(){
			hydrogenDioxide.fadeIn(500);
		}, 8000);
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			hideEverythingBut($("#frame-012"));
			sendCompletedStatement(11);
		}, 16000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
}

launch["frame-012"] = function(){
		theFrame = $("#frame-012"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		griddles = $(prefix + " .griddles"),
		recipeTitle = $(prefix + " .recipe-title"),
		recipe = $(prefix + " .recipe"),
		griddlesResult = $(prefix + " .griddles-result"),
		relativeMassEng = $(prefix + " .relative-mass-eng"),
		relativeMassRus = $(prefix + " .relative-mass-rus"), 
		relativeMassKaz = $(prefix + " .relative-mass-kaz"),
		formulaAnimated = $(prefix + " .formula-animated"),
	
	naclSprite = new Motio(recipe[0], {
		fps: 3,
		frames: 26
	});
	
	naclSprite.on("frame", function(){
		if(this.frame === 10)
		{
			this.pause();
			audio[2].play();
		}
		if(this.frame === 18)
		{
			this.pause();
		}
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 4
	});
	
	formulaSprite = new Motio(formulaAnimated[0], {
		fps: 1,
		frames: 4
	});
	
	formulaSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	doorOpen.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	griddles.fadeOut(0);
	recipeTitle.fadeOut(0);
	recipe.fadeOut(0);
	griddlesResult.fadeOut(0);
	relativeMassRus.fadeOut(0);
	relativeMassEng.fadeOut(0);
	relativeMassKaz.fadeOut(0);
	formulaAnimated.fadeOut(0);
	
	audio[0] = new Audio("audio/s5-2.mp3");
	audio[1] = new Audio("audio/s5-3.mp3");
	audio[2] = new Audio("audio/s5-4.mp3");
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(0);
		timeout[0] = setTimeout(function(){
			recipe.fadeIn(1000);
			recipeTitle.fadeIn(1500);
			griddlesResult.fadeIn(1500);
			audio[1].play();
		}, 1000);
	});
	
	audio[1].addEventListener("ended", function(){
		recipe.animate({
			"left": "35%"
		}, 1000);
		recipeTitle.fadeOut(1000);
		griddles.fadeOut(1000);
		griddlesResult.fadeOut(1000);
		naclSprite.play();
	});
	
	audio[2].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 6 && currTime != doneSecond)
		{
			doneSecond = currTime;
			naclSprite.play();
		}
		else if(currTime === 10 && currTime != doneSecond)
		{
			doneSecond = currTime;
			naclSprite.play();
		}
		else if(currTime === 15 && currTime != doneSecond)
		{
			doneSecond = currTime;
			relativeMassRus.fadeIn(0);
			timeout[0] = setTimeout(function(){
				relativeMassEng.fadeIn(0);
			}, 1000);
			timeout[0] = setTimeout(function(){
				relativeMassKaz.fadeIn(0);
			}, 2000);
		}
		else if(currTime === 20 && currTime != doneSecond)
		{
			doneSecond = currTime;
			relativeMassRus.fadeOut(0);
			relativeMassEng.fadeOut(0);
			relativeMassKaz.fadeOut(0);
			recipe.fadeOut(0);
			formulaAnimated.fadeIn(0);
			formulaSprite.play();
		}
	});
	
	audio[2].addEventListener("ended", function(){
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(12);
		}, 3000);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		griddles.fadeIn(500);
	}
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-013"] = function(){
		theFrame = $("#frame-013"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		task1 = $(prefix + " .task-1"), 
		task2 = $(prefix + " .task-2"), 
		task3 = $(prefix + " .task-3"),
		everything = $(prefix + " .task"),
		buttons = $(prefix + " .button"),
		abacus = $(prefix + " .abacus"),
		resultLabel = $(prefix + " .result-label"), 
	
	activeQuestion = 2;
	
	audio[0] = new Audio("audio/s5-51.mp3");
	audio[1] = new Audio("audio/s5-53.mp3");
	audio[2] = new Audio("audio/s5-52.mp3");
	
	var boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(500);
	});
	
	audio[1].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(500);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	boy.fadeOut(0);
	everything.fadeOut(0);
	$(prefix + " .task-"+activeQuestion).fadeIn(0);
	
	startButtonListener = function(){
		boy.fadeIn(0);
		boySprite.play();
		audio[2].play();
		timeout[0] = setTimeout(function(){
			boySprite.pause();
			boy.fadeOut(500);
		}, 3000);
	}
		
	var buttonsListener = function(){
		if($(this).attr("data-correct"))
		{
			activeQuestion --;
			everything.fadeOut(0);
			$(prefix + " .task-"+activeQuestion).fadeIn(0);
			boy.fadeIn(0);
			boySprite.play();
			audio[0].play();
		}
	};
	
	buttons.off("click", buttonsListener);
	buttons.on("click", buttonsListener);
	
	var task1Draggables = $(prefix + " .task-1.ball"),
		task1Draggabillies = [], vegetable, basket;
	
	for (var i = 0; i < task1Draggables.length; i ++)
		task1Draggabillies[i] = new Draggabilly(task1Draggables[i]);
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
	};
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			basket.html(vegetable.html());
			vegetable.remove();
			
			if(!$(prefix + " .task-1.ball").length)
			{
				activeQuestion --;
				everything.fadeOut(0);
				$(prefix + " .task-"+activeQuestion).fadeIn(0);
				if(activeQuestion === 0)
				{
					abacus.css({
						"width": "1%",
						"height": "1%",
						"left": "0%",
						"top": "0%"});
					
					resultLabel.css({
						"opacity": "0",
						"width": "0%",
						"height": "0%"
					});
					
					timeout[0] = setTimeout(function(){
						abacus.addClass("transition-2s");
						resultLabel.addClass("transition-2s");
						resultLabel.css({
							"opacity": "",
							"width": "",
							"height": ""
						});
						timeout[1] = setTimeout(function(){
							abacus.css({
								"width": "",
								"height": "",
								"left": "",
								"top": ""});
							audio[1].play();
							boy.fadeIn(0);
							boySprite.play();
							timeout[2] = setTimeout(function(){
								abacus.css({
									"width": "10%",
									"height": "20%",
									"left": "90%",
									"top": "80%"
								});
								theFrame.attr("data-done", "true"); 
								fadeNavsInAuto();
								sendCompletedStatement(13);
							},6000);
						}, 1000);
					}, 1000);
				}
			}
		}
		else
		{
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}
	};
	
	for (var i = 0; i < task1Draggabillies.length; i ++)
	{
		task1Draggabillies[i].on("dragStart", onStart);
		task1Draggabillies[i].on("dragEnd", onEnd);
	}

	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-014"] = function(){
		theFrame = $("#frame-014"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		boy = $(prefix + " .boy"),
		barrel = $(prefix + " .barrel"), 
		lockOpen = $(prefix + " .lock-open"),
		lockClosed = $(prefix + " .lock-closed"),
		abacus = $(prefix + " .abacus"),
		taskLabel = $(prefix + " .task-label"),
		lockCenter = $(prefix + " .lock-center-sprite"),
		lockNums = $(prefix + " .lock-num"),
		lockNumImage1 = $(prefix + " .lock-num-1"),
		lockNumImage2 = $(prefix + " .lock-num-2"),
		lockNumImage3 = $(prefix + " .lock-num-3"),
		lockNumImage4 = $(prefix + " .lock-num-4"),
		lockButton = $(prefix + " .lock-button"),
		lockNumButtons = $(prefix + " .lock-num-button"),
		lockNumButton1 = $(prefix + " .lock-num-button-1"),
		lockNumButton2 = $(prefix + " .lock-num-button-2"),
		lockNumButton3 = $(prefix + " .lock-num-button-3"),
		lockNumButton4 = $(prefix + " .lock-num-button-4"),
		lockNum1 = 1, 
		lockNum2 = 1, 
		lockNum3 = 1, 
		lockNum4 = 1,
	
	lockNum1Sprite = new Motio(lockNumImage1[0], {
		fps: 1,
		frames: 10
	});
	lockNum2Sprite = new Motio(lockNumImage2[0], {
		fps: 1,
		frames: 10
	});
	lockNum3Sprite = new Motio(lockNumImage3[0], {
		fps: 1,
		frames: 10
	});
	lockNum4Sprite = new Motio(lockNumImage4[0], {
		fps: 1,
		frames: 10
	});
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	barrelSprite = new Motio(barrel[0], {
		fps: 1,
		frames: 3
	});
	
	lockOpenSprite = new Motio(lockOpen[0], {
		fps: 1,
		frames: 2
	});
	
	lockClosedSprite = new Motio(lockClosed[0], {
		fps: 1,
		frames: 2
	});
	
	lockCenterSprite = new Motio(lockCenter[0], {
		fps: 1, 
		frames: 2
	});
	
	audio[0] = new Audio("audio/s6-1.mp3");
	audio[1] = new Audio("audio/water-sound.mp3");
	audio[2] = new Audio("audio/scale-sound.mp3");
	//audio[3] = new Audio("audio/super.mp3");
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(0);
	});
	
	lockOpen.fadeOut(0);
	lockCenter.fadeOut(0);
	lockNums.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	lockNumButtons.fadeOut(0);
	bg2.fadeOut(0);
	
	lockClosedSprite.to(2, true);
	
	var lockButtonListener = function(){
		boy.fadeOut(0);
		lockCenter.fadeIn(0);
		lockNums.fadeIn(0);
		lockNumButtons.fadeIn(0);
	};
	
	lockButton.click(lockButtonListener);
	
	var lockNumButtonsListener = function(){
		if($(this).hasClass("lock-num-button-1"))
		{	
			lockNum1 ++;
			if(lockNum1 > 10)
				lockNum1 = 1;
			lockNum4Sprite.to(lockNum1 - 1, true);
		}
		if($(this).hasClass("lock-num-button-2"))
		{	
			lockNum2 ++;
			if(lockNum2 > 10)
				lockNum2 = 1;
			lockNum3Sprite.to(lockNum2 - 1, true);
		}
		if($(this).hasClass("lock-num-button-3"))
		{	
			lockNum3 ++;
			if(lockNum3 > 10)
				lockNum3 = 1;
			lockNum2Sprite.to(lockNum3 - 1, true);
		}
		if($(this).hasClass("lock-num-button-4"))
		{	
			lockNum4 ++;
			if(lockNum4 > 10)
				lockNum4 = 1;
			lockNum1Sprite.to(lockNum4 - 1, true);
		}
		
		if(lockNum1 === 2 && lockNum2 === 10
			&& lockNum3 === 10 && lockNum4 === 10)
			{
				lockCenter.css("background-position", "100% 0%");
				audio[2].play();
				timeout[0] = setTimeout(function(){
					//audio[3].play();
					lockCenter.fadeOut(0);
					lockNums.fadeOut(0);
					lockNumButtons.fadeOut(0);
					lockClosed.fadeOut(0);
				}, 2000);
				timeout[1] = setTimeout(function(){
					audio[1].play();
					bg1.fadeOut(2000);
					bg2.fadeIn(2000);
				}, 3000);
				timeout[2] = setTimeout(function(){
					theFrame.attr("data-done", "true"); 
					fadeNavsInAuto();
					sendLaunchedStatement(14);
				}, 6000);
			}	
	}
	lockNumButtons.off("click", lockNumButtonsListener);
	lockNumButtons.on("click", lockNumButtonsListener);	
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		timeout[0] = setTimeout(function(){
			barrelSprite.to(1, true);
		}, 10000);
		timeout[3] = setTimeout(function(){
			boySprite.pause();
		}, 9000);
		timeout[4] = setTimeout(function(){
			boySprite.play();
		}, 10000);
		timeout[5] = setTimeout(function(){
			barrelSprite.to(2, true);
		}, 12000);
		timeout[6] = setTimeout(function(){
			barrelSprite.to(0, true);
		}, 13000);
		timeout[7] = setTimeout(function(){
			lockClosedSprite.to(1, true);
		}, 20000);
		timeout[7] = setTimeout(function(){
			lockClosedSprite.to(0, true);
		}, 20500);
	};
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(14);
	}, 2000);
}

launch["frame-015"] = function(){
		theFrame = $("#frame-015"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		jug = $(prefix + " .jug"),
		keepThinking = $(prefix + " .keep-thinking"),
		key = $(prefix + " .key"),
		clayExit = $(prefix + " .clay-exit"), 
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	clayExitSprite = new Motio(clayExit[0], {
		fps: 2,
		frames: 9
	});
	
	clayExitSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(15);
		}
	});
	
	doorOpen.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	keepThinking.fadeOut(0);
	clayExit.fadeOut(0);
	
	audio[0] = new Audio("audio/s6-2.mp3");
	audio[3] = new Audio("audio/footsteps.mp3");
	audio[4] = new Audio("audio/door-open.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime != doneSecond)
		{
			doneSecond = currTime;
			jug.css("background-position", "100%");
			timeout[2] = setTimeout(function(){
				jug.css("background-position", "");
			},2000);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(500);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
	}
	
	var jugListener = function()
	{
		if($(this).attr("data-correct"))
		{
			key.addClass("transition-2s");
			key.css("left", "9%");
			key.css("top", "60%");
			timeout[0] = setTimeout(function(){
				audio[4].play();
				clayExit.fadeIn(0);
				timeout[1] = setTimeout(function(){
					clayExitSprite.play();
					audio[3].play();
				}, 1000);
			}, 3000);
		}
		else
		{
			keepThinking.fadeIn(0);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(0);
			}, 2000);
		}
	}
	
	jug.off("click", jugListener);
	jug.on("click", jugListener);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(15);
	}, 2000);
}

launch["frame-016"] = function(){
		theFrame = $("#frame-016"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		photoFrame = $(prefix + ".photo-frame"),
		farewell = $(prefix + ".farewell"),
		chestOpen = $(prefix + " .chest-open"),
		chestClosed = $(prefix + " .chest-closed"),
		coinAnimated = $(prefix + " .coin-animated"),
		coinInfoAnimated = $(prefix + " .coin-info-animated"),
		hand = $(prefix + " .hand"),
		abacusJug = $(prefix + " .abacus, " + prefix + " .jug"),
		abacusAnimated = $(prefix + " .abacus-animated"),
		abacusCount = 0,
		woodButton = $(prefix + ".wood-button"),
		jugButton = $(prefix + ".jug-button"),
		coinButton = $(prefix + ".coin-button"),
		buttons = $(prefix = ".button"),
		jug = $(prefix + " .jug"),
	
	abacusSprite = new Motio(abacusAnimated[0], {
		fps: 1,
		frames: 15
	});
	
	handSprite = new Motio(hand[0], {
		fps: 10, 
		frames: 12
	});
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	coinInfoSprite = new Motio(coinInfoAnimated[0], {
		fps: 1/2,
		frames: 5
	});
	
	handSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			this.to(0, true);
		}
	});
	
	audio[0] = new AudioPiece("s8-1", 0, 37);
	audio[1] = new Audio("audio/chest-close.mp3");
	audio[2] = new Audio("audio/coins-sound.mp3");
	audio[3] = new AudioPiece("s8-1", 65, 100);
	audio[4] = new Audio("audio/main-theme.mp3");
	audio[5] = new AudioPiece("s8-1", 58, 62);
	
	var coinListener = function(){
		audio[2].play();
		handSprite.play();
		abacusAnimated.fadeIn(0);
		abacusJug.fadeOut(0);
		abacusCount ++;
		abacusSprite.to(abacusCount, true);
	};
	
	var jugListener = function(){
		audio[2].play();
		abacusAnimated.fadeIn(0);
		abacusJug.fadeOut(0);
		if(abacusCount >= 0)
			abacusCount --;
		abacusSprite.to(abacusCount, true);
	}
	
	var woodListener = function(){
		if(abacusCount === 10)
		{
			audio[3].play();
			theFrame.css("background-image", "");
			boy.fadeIn(0);
			photoFrame.fadeIn(0);
			boySprite.play();
			jug.fadeIn(0);
			jug.css({
				"width": "25%",
				"height": "50%",
				"left": "35%", 
				"top": "25%"
			});
			hand.fadeOut(0);
			abacusAnimated.fadeOut(0);
			timeout[1] = setTimeout(function(){
				boySprite.pause();
				boy.addClass("transition-2s");
				photoFrame.addClass("transition-2s");
				boy.css("opacity", "0");
				photoFrame.css("opacity", "0");
				farewell.fadeIn(1500);
				theFrame.css("background-image", "url(pics/s0-bg-2.jpg)");
				theFrame.addClass("transition-3s");
				audio[4].play();
				timeout[0] = setTimeout(function(){
					theFrame.css("opacity", "0");
					soundToZero(audio[4], 5);
				}, 5000);
				timeout[0] = setTimeout(function(){
					sendCompletedStatement(16);
					theFrame.attr("data-done", "true"); 
					fadeNavsIn();
				}, 4000);
			}, 3000);
		}
		else
			audio[5].play();
	}
	
	var changeView = function(){
		chestOpen.fadeOut(0);
		coinAnimated.fadeOut(0);
		coinInfoAnimated.fadeOut(0);
		theFrame.css("background-image", "url(pics/chest-inside.png)");
		theFrame.css("background-size", "100%");
		hand.fadeIn(0);
		buttons.fadeIn(0);
		coinButton.off("click", coinListener);
		coinButton.on("click", coinListener);
		woodButton.off("click", woodListener);
		woodButton.on("click", woodListener);
		jugButton.off("click", jugListener);
		jugButton.on("click", jugListener);
	};
	
	var chestListener = function(){
		chestClosed.fadeOut(0);
		chestOpen.fadeIn(0);
		audio[1].play();
		timeout[1] = setTimeout(changeView, 2000);
	}
	
	fadeNavsOut();
	fadeLauncherIn();
	chestOpen.fadeOut(0);
	coinAnimated.fadeOut(0);
	coinInfoAnimated.fadeOut(0);
	hand.fadeOut(0);
	abacusAnimated.fadeOut(0);
	buttons.fadeOut(0);
	farewell.fadeOut(0);
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		timeout[0] = setTimeout(function(){
			chestOpen.fadeIn(0);
			chestClosed.fadeOut(0);
		}, 5000);
		timeout[1] = setTimeout(function(){ boySprite.pause(); }, 4200);
		timeout[2] = setTimeout(function(){ boySprite.play(); }, 5200);
		timeout[3] = setTimeout(function(){
			chestOpen.fadeOut(0);
			chestClosed.fadeIn(0);
			chestClosed.html("0,63 моль алтын(Au)");
			boySprite.pause();
		}, 10000);
		timeout[4] = setTimeout(function(){ boySprite.pause(); }, 11500);
		timeout[5] = setTimeout(function(){ boySprite.play(); }, 12500);
		timeout[6] = setTimeout(function(){
			chestClosed.addClass("transition-2s");
			chestClosed.css("left", "70%");
			chestOpen.css("left", "70%");
			coinAnimated.fadeIn(500);
			coinInfoAnimated.fadeIn(0);
			coinInfoSprite.play();
			boySprite.play();
		}, 15000);
		timeout[7] = setTimeout(function(){ boySprite.pause(); }, 16200);
		timeout[8] = setTimeout(function(){ boySprite.play(); }, 17200);
		timeout[9] = setTimeout(function(){ boySprite.pause(); }, 25200);
		timeout[10] = setTimeout(function(){ boySprite.play(); }, 26000);
		timeout[11] = setTimeout(function(){ boySprite.pause(); }, 30300);
		timeout[12] = setTimeout(function(){ boySprite.play(); }, 31000);
		timeout[13] = setTimeout(function(){
			boySprite.pause();
			boy.fadeOut(500);
			photoFrame.fadeOut(500);
			coinAnimated.fadeOut(500);
			coinInfoAnimated.fadeOut(0);
			chestClosed.off("click", chestListener);
			chestClosed.on("click", chestListener);
		}, 37000);
	}
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(16);
	}, 2000);
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	elemId = elem.attr("id");
	regBox = $(".reg-box");
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	
	
	for (var i = 0; i < audioPiece.length; i++)
		audioPiece[i].pause();	

	for (var i = 0; i < timeout.length; i++)
	{
		console.log(timeout[i]);
		window.clearTimeout(timeout[i]);
	}
	
	for (var i = 0; i < sprites.length; i++)
		sprites[i].pause();
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	launch[elemId]();
	initMenuButtons();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
		buttonSound.play();
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	//video[0].play();
	
	//timeout[0] = setTimeout(function(){
		video.hide();
	//}, 10000);
	//timeout[1] = setTimeout(function(){
		pic.hide(); 
	//}, 15000);
}

$(document).ready(main);